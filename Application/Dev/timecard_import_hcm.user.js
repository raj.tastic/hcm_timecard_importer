// ==UserScript==
// @name         Timesheet Importer HCM
// @namespace    http://tampermonkey.net/
// @version      1.00
// @description  Allow TSV Import for timesheets
// @author       Roshan Gonsalkorale (roshan.gonsalkorale@oracle.com)
// @include        https://eeho.fa.us2.oraclecloud.com/*
// @grant        none

// ==/UserScript==

(function() {
  'use strict';

  /*
  ##############################
  ### A. DECLARE GLOBAL VARS ###
  ##############################
  */

  // Global Vars
  window._ui_hack = window._ui_hack || {};
  window._ui_hack.fn = window._ui_hack.fn || {};
  window._ui_hack.vars = window._ui_hack.vars || {};
  window._ui_hack.fn.config = _ui_hack.fn.config || {};

  /*
  ##############################
  ### B. CONFIG ###
  ##############################
  */

  // Flag environment
  _ui_hack.vars.env = "dev"; // or 'prod'
  _ui_hack.vars.version = "1.00";
  _ui_hack.vars.application = "Timesheet Importer (HCM)";

  // File Parsing Options
  _ui_hack.fn.config.papaParse = {

    header: true,
    complete: function(results, file) {
      _ui_hack.fn.fileProcessing(results, file);
    }
  }

  // Infinity Tracking Object
  _ui_hack.vars.userIdTracking = true; // Set to false to not track email address/userId
  _ui_hack.vars.infi_tracking = true; // Set to false to turn off Infinity Tracking  

  /*
  ############################
  ### C. DECLARE FUNCTIONS ###
  ############################
  */

  // FUNCTIONS

  // Fn:triggerError()
  _ui_hack.fn.triggerError = function(errorName, errorDetails, err) {
    console.log("UI Hack:Error:" + errorName, errorDetails);
    console.log("UI Hack:Error:Details:" + errorName, err);
    alertify.errorTrigger("<strong>" + errorName + "</strong><br>" + errorDetails);
    _ui_hack.fn.InfinityTrack("click", errorName, "Errors", errorDetails, {
      "errorVerbose": err
    }); // trackType,event_action,event_category,event_label,additionalTrackingObject
  }
  _ui_hack.fn.triggerErrorSilent = function(errorName, errorDetails, err) {
    console.log("UI Hack:Error:Silent:" + errorName, errorDetails);
    console.log("UI Hack:Error:Silent:Details:" + errorName, err);    
    _ui_hack.fn.InfinityTrack("click", errorName, "Errors", errorDetails, {
      "errorVerbose": err
    }); // trackType,event_action,event_category,event_label,additionalTrackingObject
  }

  // Fn:toType()
  _ui_hack.fn.toType = function(obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
  }

  // Fn:InfinityLibraryAdd()
  _ui_hack.fn.InfinityLibraryAdd = function() {

    try{
      if (_ui_hack.vars.infi_tracking) {

        // Callback upon library load
        var callback = function() {

          // Infi Helper 1.0
          window.infi_helper = window.infi_helper || [];

          // Process data      
          for (var i = 0; i < infi_helper.length; i++) {

            let data = infi_helper[i]

            if (window.ORA && ORA[data.eventType] && data.data) {
              ORA[data.eventType]({
                "data": data.data
              });

            }
            infi_helper.pop();

          }

          // Override Function
          infi_helper.push = function(data) {

            if (window.ORA && ORA[data.eventType] && data.data) {
              ORA[data.eventType]({
                "data": data.data
              });
            }

            return Array.prototype.push.apply(this, arguments);
          }
        }
        _ui_hack.fn.fileAdder("script", "//c.oracleinfinity.io/acs/account/hsj8iasxuf/js/AndyDemoTag/odc.js", callback);
      }
    } catch(error){
      _ui_hack.fn.triggerErrorSilent("Infinity Library Add Error", "There's a problem with _ui_hack.fn.InfinityLibraryAdd() function" , error);
    }

  }

  // Fn:InfinityTrack()
  _ui_hack.fn.InfinityTrack = function(trackType, event_action, event_category, event_label, additionalTrackingObject) {

    // Create Standard Tracking Vars
    _ui_hack.vars.infi_tracking_object = {};
    _ui_hack.vars.infi_tracking_object["app"] = _ui_hack.vars.application;
    _ui_hack.vars.infi_tracking_object["app_version"] = _ui_hack.vars.version;
    _ui_hack.vars.infi_tracking_object["app_env"] = _ui_hack.vars.env;
    if (window.userName && _ui_hack.vars.userIdTracking) {
      _ui_hack.vars.infi_tracking_object["username"] = window.userName;
    }
    if (window.userId && _ui_hack.vars.userIdTracking) {
      _ui_hack.vars.infi_tracking_object["userId"] = window.userId;
    }

    // Create Event Tracker
    var eventTracker = {};
    if (event_label) {
      eventTracker["event_label"] = event_label;
    };
    if (event_action) {
      eventTracker["event_action"] = event_action;
    };
    if (event_category) {
      eventTracker["event_category"] = event_category;
    };

    // Add additional tracking vars if available
    if (additionalTrackingObject) {
      Object.assign(eventTracker, additionalTrackingObject);
    }

    // Update with standard vars
    Object.assign(eventTracker, _ui_hack.vars.infi_tracking_object);

    // Update all vars in object to include "ORA."    
    var eventTrackerOra = {};

    for (const attrName in eventTracker) {
      eventTrackerOra["ORA." + attrName] = eventTracker[attrName];
    }

    // Push Tracker    
    if (_ui_hack.vars.infi_tracking) {

      // Create infi namespace
      window.infi_helper = window.infi_helper || [];

      // Send Data
      console.log("UI Hack:fn:InfinityTrack:eventFire:" + trackType, eventTrackerOra);
      infi_helper.push({
        "eventType": trackType,
        "data": eventTrackerOra
      });
    } else {
      console.log("UI Hack:fn:InfinityTrack:eventSuppress:" + trackType, eventTrackerOra);
    }

  }

  // Fn:pageChecker()
  _ui_hack.fn.pageChecker = function() {

    console.log("UI Hack:fn:pageChecker() : triggered");

    // Declare Config Vars

    // Flag Libary Vars
    _ui_hack.vars.checks = {};
    _ui_hack.vars.checks.libraries = {};
    _ui_hack.vars.checks.libraries.alertify = {};
    _ui_hack.vars.checks.libraries.alertify.requested = false;
    _ui_hack.vars.checks.libraries.alertify.available = false;
    _ui_hack.vars.checks.libraries.Papa = {};
    _ui_hack.vars.checks.libraries.Papa.requested = false;
    _ui_hack.vars.checks.libraries.Papa.available = false;

    // Flag Script Vars
    _ui_hack.vars.checks.timecard_script = {};
    _ui_hack.vars.checks.timecard_script.started = false;
    _ui_hack.vars.checks.timecard_script.completed = false;

    // Flag checks
    _ui_hack.vars.checks.page = {};

    // Select the node that will be observed for mutations
    const targetNode = document.body;

    // Options for the observer (which mutations to observe)
    const config = {
      attributes: true,
      childList: true,
      subtree: true
    };

    // Callback function to execute when mutations are observed
    const callback = function(mutationsList, observer) {

      // Timecard Page Check : Check if Import Button Available
      if (document.querySelector("[title='Import']")) {

        _ui_hack.vars.checks.page.import_button = true;

      } else {
        _ui_hack.vars.checks.page.import_button = false;
      }

      // Timecard Page Check : Next Button Available?
      if (document.querySelector("[title='Next']") && document.querySelector("[title='Next']").innerText == "Next") {
        _ui_hack.vars.checks.page.next_button = true;
      } else {
        _ui_hack.vars.checks.page.next_button = false;
      }

      // Timecard Page Check : Timecard H1 Available?
      if (document.querySelector("h1") && (document.querySelector("h1").textContent.indexOf("Edit Time Card") == 0 || document.querySelector("h1").textContent.indexOf("Create Time Card:") == 0)) {
        _ui_hack.vars.checks.page.timecard_h1 = true;
      } else {
        _ui_hack.vars.checks.page.timecard_h1 = false;
      }

      // Timecard Page Check : Flag if on timecard page or not        
      if (_ui_hack.vars.checks.page.timecard_h1 && _ui_hack.vars.checks.page.next_button) {

        _ui_hack.vars.checks.page.timecard_page = true;
      } else {

        _ui_hack.vars.checks.page.timecard_page = false;

      }

      // If on timecard page...
      if (_ui_hack.vars.checks.page.timecard_page) {

        // If libraries not requested yet...
        if (!_ui_hack.vars.checks.libraries.alertify.requested || !_ui_hack.vars.checks.libraries.Papa.requested) {

          // Import Libraries
          //_ui_hack.fn.fileAdder("css", "https://cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"); // Alertify CSS() : Default
          _ui_hack.fn.fileAdder("script", "//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"); // Alertify JS            
          _ui_hack.fn.fileAdder("css", "//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.rtl.min.css"); // Alertify CSS : RTL
          _ui_hack.fn.fileAdder("script", "//cdnjs.cloudflare.com/ajax/libs/PapaParse/5.3.0/papaparse.min.js"); // papaParse
          _ui_hack.vars.checks.libraries.alertify.requested = true;
          _ui_hack.vars.checks.libraries.Papa.requested = true;

        }

        // Check if libraries available...flag it
        if (window.Papa && window.alertify) {
          _ui_hack.vars.checks.libraries.alertify.available = true;
          _ui_hack.vars.checks.libraries.Papa.available = true;

        }

        // Check if never run and conditions met...
        if (_ui_hack.vars.checks.page.timecard_page && _ui_hack.vars.checks.libraries.alertify.available && _ui_hack.vars.checks.libraries.Papa.available) {

          // Check if script run yet
          if (!_ui_hack.vars.checks.timecard_script.started) {

            // Flag script started
            _ui_hack.vars.checks.timecard_script.started = true;

            // Trigger Script  
            console.log("UI Hack:fn:pageChecker() : timecard page detected and not hacked...first time...");
            _ui_hack.fn.triggerScript();

            // Check already run but not available anymore...
          } else if (_ui_hack.vars.checks.timecard_script.completed && !_ui_hack.vars.checks.page.import_button) {

            // unflag script completed
            _ui_hack.vars.checks.timecard_script.completed = false;

            // Trigger Script  
            console.log("UI Hack:fn:pageChecker() : timecard page detected and not hacked...repeat...");
            _ui_hack.fn.triggerScript();


          }
        }
      }
    }


    // Create an observer instance linked to the callback function
    const observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);

  }

  // Fn:fileAdder
  _ui_hack.fn.fileAdder = function(type, src, callback) {
    if (type === "css") {

      var file = document.createElement("link");
      file.setAttribute("rel", "stylesheet");
      file.setAttribute("href", src);
      console.log("UI Hack:fn:fileAdder() : " + src + " added");

    } else {

      var file = document.createElement(type);

      //file.setAttribute("src", src);
      file.src = src;
      console.log("UI Hack:fn:fileAdder() : " + src + " added");
      if (typeof callback !== "undefined") {
        //file.setAttribute("onload", callback());
        file.onload = function() {
          callback()
        };
      };

    }

    document.body.appendChild(file);

  };

  // Fn:populateRow()
  _ui_hack.fn.populateRow = function(rowNumber, matrix_ref, rowObject) {

    console.log("UI Hack:fn:populateRow: triggered");

    // Rebuild row in lower-case        
    var newRowObject = {};
    for (var eachColumn in rowObject) {
      newRowObject[eachColumn.toLowerCase()] = rowObject[eachColumn];
    }


    try {

      // Project
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":socMatrixAttributeNumber2::content']").value = newRowObject.project;

      // Task
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":socMatrixAttributeNumber3::content']").value = newRowObject.task;

      // Hours      
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":m1::content']").value = newRowObject.sat;
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":m2::content']").value = newRowObject.sun;
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":m3::content']").value = newRowObject.mon;
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":m4::content']").value = newRowObject.tue;
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":m5::content']").value = newRowObject.wed;
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":m6::content']").value = newRowObject.thu;
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":m7::content']").value = newRowObject.fri;

      // Country
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":socMatrixAttributeChar6::content']").value = newRowObject.country;

      // State
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":socMatrixAttributeChar7::content']").value = newRowObject.state;

      // Expenditure
      document.querySelector("input[id='" + matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + rowNumber + ":socMatrixAttributeAltName20::content']").value = newRowObject["expenditure type"];

    } catch (error) {
      _ui_hack.fn.triggerError("Row Population Error", "Something went wrong with parsing your row of data - take a look at your sheet again", error);
    }

  }

  // Fn:Create Button
  _ui_hack.fn.addButton = function(buttonName, insertSelector, clickFunction) {
    
    console.log("UI Hack:fn:addButton: triggered");
    try {
      var importButton = document.createElement("div");

      var importButtonClass = document.querySelector(insertSelector).className;

      importButton.setAttribute("id", "ImportCSV");
      importButton.setAttribute("title", "Import");
      importButton.setAttribute("class", importButtonClass);
      importButton.setAttribute("_afrgrp", "0");
      importButton.setAttribute("role", "presentation");
      if (clickFunction) {
        importButton.setAttribute("onclick", clickFunction + "()")
      }

      var importButtonHref = document.createElement("a")
      //importButtonHref.setAttribute("href","");
      var importButtonHrefClass = document.querySelector(insertSelector).firstElementChild.className;
        
      importButtonHref.setAttribute("class", importButtonHrefClass);
      importButtonHref.setAttribute("role", "button");
      importButtonHref.innerText = buttonName;

      importButton.appendChild(importButtonHref);
      
      document.querySelector(insertSelector).parentNode.insertBefore(importButton, null);
    } catch(error){
      _ui_hack.fn.triggerErrorSilent("Add Button Error - '" + buttonName + "'", "Problem _ui_hack.fn.addButton()" , error);
    }

  }

  // Fn:Create Upload HTML
  _ui_hack.fn.addUploadHTML = function(selector) {

    try {
      var uploadHTML = document.createElement("input");
      uploadHTML.setAttribute("type", "file");
      uploadHTML.setAttribute("id", "fileUpload");
      uploadHTML.addEventListener('change', function() {
        _ui_hack.fn.fileProcessStart(this.files);
        document.querySelector("#fileUpload").value = "";

      });
      selector.append(uploadHTML);
    } catch(error){
      _ui_hack.fn.triggerErrorSilent("Add Upload HTML Error", "There's a problem with _ui_hack.fn.addUploadHTML() function" , error);
    }
  }

  // Fn:fileProcessStart()
  _ui_hack.fn.fileProcessStart = function(files) {
    try{
      console.log("UI Hack:fileProcessStart() : trigger");

      Papa.parse(files[0], _ui_hack.fn.config.papaParse)
    } catch(error){
      _ui_hack.fn.triggerErrorSilent("File Process Start Error", "There's a problem with _ui_hack.fn.fileProcessStart() function" , error);
    }
  }

  _ui_hack.fn.fileProcessing = function(results, file) {

    console.log("UI Hack:fileProcessing() : trigger");

    try {

      if (!file.name.endsWith(".tsv")) {
        _ui_hack.fn.triggerError("Wrong File Type", "Ensure you are importing a TAB-delimited file (your file name is '" + file.name + "' - expected to end with '.tsv'");
        return;
      }

      // Check Number of Rows Available    
      for (var i = 0; i < (i + 1); i++) {

        if (!document.querySelector("input[id='" + _ui_hack.vars.matrix_ref + ":AP1:r3:0:AT2:_ATp:ATt2:" + i + ":socMatrixAttributeNumber2::content']")) {
          var available_rows = i;
          break;
        }

      }

      // Check Required Rows
      let required_rows = results.data.length;

      if (required_rows > available_rows) {
        _ui_hack.fn.triggerError("Not Enough Rows", "Please add more rows in your timesheet");
        return
      }

      // Check for correct column headers and report error if missing
      var columnHeadersRequired = {};
      var columnHeadersMissing = [];

      columnHeadersRequired.project = true;
      columnHeadersRequired.task = true;
      columnHeadersRequired.sat = true;
      columnHeadersRequired.sun = true;
      columnHeadersRequired.mon = true;
      columnHeadersRequired.tue = true;
      columnHeadersRequired.wed = true;
      columnHeadersRequired.thu = true;
      columnHeadersRequired.fri = true;
      columnHeadersRequired.country = true;
      columnHeadersRequired.state = true;
      columnHeadersRequired["expenditure type"] = true;

      // Force to lower-case before checking
      var first_row = {};
      for (var eachColumn in results.data[0]) {
        first_row[eachColumn.toLowerCase()] = results.data[0][eachColumn];
      }

      // Check for missing columns
      for (var headerName in columnHeadersRequired) {
        if (typeof first_row[headerName.toLowerCase()] == "undefined") {
          columnHeadersMissing.push(headerName);
        }
      }

      // trigger error if error
      if (columnHeadersMissing.length > 0) {
        _ui_hack.fn.triggerError("Header(s) Missing", columnHeadersMissing.join("|"));
        return;
      };


      // Process Rows
      for (var i = 0; i < results.data.length; i++) {
        let row_number = i;
        let row_data = results.data[i];
        _ui_hack.fn.populateRow(row_number, _ui_hack.vars.matrix_ref, row_data);
      }

      alertify.success("Timecard successfully imported!");
      _ui_hack.fn.InfinityTrack("click", "Timecard Import Success", "OK"); // trackType,event_action,event_category,event_label,additionalTrackingObject
    } catch(error){
      _ui_hack.fn.triggerErrorSilent("File Processing Error", "There's a problem with _ui_hack.fn.fileProcessing() function" , error);
    }

  }

  // Fn:importDataButtonClick()
  _ui_hack.fn.importDataButtonClick = function() {

    console.log("UI Hack:fn:importDataButtonClick: triggered");
    try{
      _ui_hack.fn.InfinityTrack("click", "Import Button Click ", "OK"); // trackType,event_action,event_category,event_label,additionalTrackingObject

      // Trigger Import UI
      alertify.importData("");

    } catch(error){
      _ui_hack.fn.triggerErrorSilent("Import Data Click Error", "There's a problem with importDataButtonClick() function" , error);
    }

  }

  // Fn:addImportUI()
  _ui_hack.fn.addImportUI = function() {

    if (!alertify.importData) {

      //define a new errorAlert base on alert
      alertify.dialog('importData', function factory() {
        return {
          build: function() {
            var importHeader = '<span class="xnr">' +
              '<strong>Import Your Timesheet</strong></span>';
            this.setHeader(importHeader);
          },
          setup: function() {
            return {
              buttons: [{
                text: "Choose File",
                key: 27,
                attrs: {
                  onclick: '_ui_hack.fn.importData()',
                  id: "uploadButton"
                },
                resizable: true
              }],
              focus: {
                element: 0
              }
            };
          },
          prepare: function() {
            this.setContent(              
              "Import your Timesheet here.<br><br>" +
              "Refer to <a target='_blank' href='https://docs.google.com/spreadsheets/d/1g1AC_laVvR54T0vowxatNtxprlsAIMqiRVGTaHBdpxA/edit?usp=sharing'>Template</a> for sample TSV.<br><br>" +
              "<strong>Tips!</strong><br><ul>" +
              "<li>Ensure you have created enough rows in your timesheet BEFORE importing!</li>" +
              "<li>Ensure you download the file as a 'TSV'!</li>" +
              "<li>Ensure you have the EXACT project/task/expenditure etc values</li></ul>" +            
              "<strong>Issues?</strong><br>" + 
              "<ul><li><a target='_blank' href='https://gitlab.com/roshan.gonsalkorale/hcm_timecard_importer/-/issues' onclick='_ui_hack.fn.InfinityTrack(&quot;click&quot;,&quot;Raise Issue&quot;,&quot;OK&quot;);'>Raise an Issue</a></li>" +
              "<li><a target='_blank' href='https://gitlab.com/roshan.gonsalkorale/hcm_timecard_importer#timecard-importer-hcm' onclick='_ui_hack.fn.InfinityTrack(&quot;click&quot;,&quot;Timecard Importer Homepage Click&quot;,&quot;OK&quot;);'>HCM Timecard Importer Homepage</a></li></ul>"
              );
              this.elements.dialog.style.height = '400px';
          }
        };
      }, true, 'alert');
    }
  }

  // Fn:addErrorHandling()
  _ui_hack.fn.addErrorHandling = function() {

    if (!alertify.errorTrigger) {

      //define a new errorAlert base on alert
      alertify.dialog('errorTrigger', function factory() {
        return {
          build: function() {
            var errorHeader = '<span class="xnr" ' +
              'style="vertical-align:middle;color:#e10000;">' +
              '</span> Error!';
            this.setHeader(errorHeader);
          },
          setup: function() {
            return {
              focus: {
                element: 0
              }
            };
          }
        };
      }, true, 'alert');
    }
  }

  // Fn:importData()
  _ui_hack.fn.importData = function() {
    console.log("UI Hack:fn:importData() : triggered");
    document.querySelector("#fileUpload").click();
  }
  _ui_hack.fn.triggerScript = function() {

    console.log("UI Hack:fn:triggerScript() : triggered");

    try{
      _ui_hack.fn.InfinityTrack("click", "Timecard Page Found", "OK"); // trackType,event_action,event_category,event_label,additionalTrackingObject

      // Grab Matrix Details
      _ui_hack.vars.matrix_ref_grab = document.querySelector(".kioskMode").getAttribute("id");
      _ui_hack.vars.matrix_ref = _ui_hack.vars.matrix_ref_grab.split(":AP1")[0];

      // Add Import Button    
      _ui_hack.fn.addButton("Import", "[title='Next']", "_ui_hack.fn.importDataButtonClick");

      // Add UI Prompt on Import Button Click
      _ui_hack.fn.addImportUI();

      // Add Error Handling
      _ui_hack.fn.addErrorHandling();

      // Add hidden file upload button
      _ui_hack.fn.addUploadHTML(document.body);

      // Flag script completed
      _ui_hack.vars.checks.timecard_script.completed = true;
    } catch(error){
      _ui_hack.fn.triggerErrorSilent("Trigger Script Error", "There's a problem with _ui_hack.fn.triggerScript() function" , error);
    }

  }

  /*
  ############################################
  ### C. TRIGGER SCRIPT WHEN PAGE IS READY ###
  ############################################
  */

  // Trigger notification that UI hack running
  console.log('UI Hack:Timecard Import HCM script running...');

  // Trigger version notification
  console.log('UI Hack:You are using the ' + _ui_hack.vars.env + ' : ' + _ui_hack.vars.version);

  // Add Infinity Tracking  
  _ui_hack.fn.InfinityLibraryAdd();
  _ui_hack.fn.InfinityTrack("click", "Script Loaded", "OK"); // trackType,event_action,event_category,event_label,additionalTrackingObject

  // Add libraries


  _ui_hack.fn.pageChecker();


})();
