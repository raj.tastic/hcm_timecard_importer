# Timecard Importer (HCM)

## Description & Demo
The importer allows you to import your timecard into the HCM timecard system. See video demo below:

**Click Below to Play Demo**

[![Demo](https://d6pdqlw297isz.cloudfront.net/t/lluYAZoe/w0-h400-playicon0/p43.f3.n0.cdn.getcloudapp.com/items/lluYAZoe/Screen%20Recording%202020-09-08%20at%2010.13.54%20am.mp4?source=thumbnail)](https://otube.oracle.com/media/Timecard+Importer+%28HCM%29/1_dqrgttsw "Demo Video")

## Installation

- Install [Tamper Monkey](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en)

### Production ###  
- Click on the following [script](https://oracle-cx-consulting-apps.s3-eu-west-1.amazonaws.com/HCM+Timecard+Importer/Prod/timecard_import_hcm.user.js) to install it.

`It will update automatically as new updates are added.`

## Beta/Dev Versions ##
You may use the beta/dev versions of the importer if you wish:

**Beta**
- Click on the following [script](https://oracle-cx-consulting-apps.s3-eu-west-1.amazonaws.com/HCM+Timecard+Importer/Beta/timecard_import_hcm.user.js) to install it.

**Dev**
- Click on the following [script](https://oracle-cx-consulting-apps.s3-eu-west-1.amazonaws.com/HCM+Timecard+Importer/Dev/timecard_import_hcm.user.js) to install it.

## How do I Work With The Google Doc Template?

You'll need to copy the [Google Doc](https://docs.google.com/spreadsheets/d/1g1AC_laVvR54T0vowxatNtxprlsAIMqiRVGTaHBdpxA/edit#gid=318883994) to use the template.

If you can, please add more options to the [Drop Down Fields](https://docs.google.com/spreadsheets/d/1g1AC_laVvR54T0vowxatNtxprlsAIMqiRVGTaHBdpxA/edit#gid=1412148566) in the Google Doc for everyone to benefit.

## How Do I Use The Import?

- Ensure you have enough rows in your timesheet for the number of rows you want to import.
- Hit 'Import'.
- Select your '.tsv' to import (ensure all of the values are **exactly** correct or HCM will hate on you).
- Import File.
- Click on one of your tasks and select a valid value
- Hit 'Next' (HCM will validate all of the values)
- Hit 'Save'
- You're done!

(if it goes wrong, just keep fiddling and re-import as HCM accepts your data)

## Raising Issues
Create an issue by raising is on the [Issues](https://gitlab.com/roshan.gonsalkorale/hcm_timecard_importer/-/issues) page.

## How do I get the Latest Version/Force Upgrade?
Tampermonkey should automatically grab the latest version of the script every day but you can change the [update frequency in Tampermonkey](https://www.tampermonkey.net/index.php?version=4.10&ext=dhdg&updated=true#script_update) if you like.

To force upgrade, just download the script again and 're-install/update'.

## Upcoming Development
See [Issues Board](https://gitlab.com/roshan.gonsalkorale/hcm_timecard_importer/-/boards/2055710) for list of issues/what's coming up.

## Releases

<h4>1.00 (2020 10 02)</h4>
- Moving to non-Oracle gitlab and adding CI/CD for automatic deployment to S3
- Moved S3 hosted script locations for CI/CD

<h4>0.25.1 (2020 09 15)</h4>
- Changing link to timecard importer and removing redundant loop code

<h4>0.25 (2020 09 15)</h4>
- Adding 'raise issue' and 'Timecard Import' call to actions (with tracking)

<h4>0.24 (2020 09 14)</h4>
- Removed setTimeout() functions and replaced with mutuation observers
- Fixed bug of multiple pageChecker() runs

<h4>0.23.1 (2020 09 14)</h4>
- Fixed broke import (caused by 0.23)

<h4>0.23 (2020 09 14)</h4>
- Allows mixed-case columns headers
- Will throw UI error if any column headers missing

<h4>0.22 (2020 09 11)</h4>
- Fixing issue with Windows not detecting file type and checking for ".tsv" extension instead of file type detection

## Maintaining the Timecard Repo ##

Project Members can read the [Wiki](https://gitlab.com/roshan.gonsalkorale/hcm_timecard_importer/-/wikis/Maintaining-the-Timecard-Importer) on how to maintain the importer.

`if you are not a project member then the above link will fail`